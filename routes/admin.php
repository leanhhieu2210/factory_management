<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AuthController;
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'AuthController@loginForm')->name('admin.login.form');
Route::post('login', 'AuthController@login')->name('admin.login.action');
Route::get('logout', 'AuthController@logout')->name('admin.logout.action');

Route::group(['middleware' => ['auth:admins']], function () {
    Route::group(['prefix' => 'staff', 'middleware' => ['permission:staff_management']], function () {
        Route::get('/list' , 'StaffController@index')->name('admin.staff.index');
        Route::get('/create' , 'StaffController@create')->name('admin.staff.create');
        Route::post('/create' , 'StaffController@store')->name('admin.staff.store');
        Route::put('delete/{id}' , 'StaffController@destroy')->name('admin.staff.destroy');
        Route::get('edit/{id}' , 'StaffController@show')->name('admin.staff.show');
        Route::put('edit/{id}' , 'StaffController@update')->name('admin.staff.edit');
        Route::get('/{id}' , 'StaffController@info')->name('admin.staff.info');
    });

    Route::group(['prefix' => 'setting', 'middleware' => ['permission:setting_general']], function () {
        Route::get('/position' , 'PositionController@index')->name('admin.setting.position');
    });

    Route::group(['prefix' => 'work', 'middleware' => ['permission:work_output']], function () {
        Route::get('' , 'WorkController@create')->name('admin.work.create');
        Route::post('' , 'WorkController@store')->name('admin.work.store');
    });

    Route::group(['prefix' => 'department', 'middleware' => ['permission:department_management']], function () {
        Route::get('' , 'DepartmentController@index')->name('admin.department.index');
        Route::get('create' , 'DepartmentController@create')->name('admin.department.create');
        Route::post('' , 'DepartmentController@store')->name('admin.department.store');
        Route::get('/{id}' , 'DepartmentController@show')->name('admin.department.show');
        Route::put('/{id}' , 'DepartmentController@update')->name('admin.department.edit');
        Route::delete('/{id}' , 'DepartmentController@destroy')->name('admin.department.destroy');
    });
});
