<?php

namespace App\Providers;

use App\Models\Permission;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            Permission::get()->map(function ($permission) {
                Gate::define($permission->name_eng, function ($user) use ($permission) {
                    return $user->hasPermission($permission);
                });
            });
        } catch (\Exception $e) {
            dd($e->getMessage());
            Log::channel('admin')->error("Fail check permission". now() .  " : " . $e->getMessage());
            abort(403);
        }
    }
}
