<?php

namespace App\Jobs;

use App\Enums\BetStatus;
use App\Models\Bet;
use App\Models\User;
use App\Traits\BetExecuteTrait;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BetExecuteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, BetExecuteTrait;

    protected $bet;
    protected $mainRoute = 'odds';
    protected $fixtureRoute = 'fixtures';
    protected $leagueRoute = 'leagues';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bet)
    {
        $this->bet = $bet;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client([
            'base_uri' => config('football.base_uri'),
            'headers' => config('football.headers'),
        ]);

        $fixture = json_decode($client->get(
            $this->fixtureRoute,
            ['query' => ['id' => $this->bet->fixture_id] ?? []]
        )->getBody()->getContents(), true);

        [$status, $code] = [null, null];

        switch ($this->bet->bet_name) {
            case 'asian-handicap' :
                [$status, $code] = $this->calAsianHandicap($this->bet, $fixture['response'][0], false);
                break;
            case 'asian-handicap-1h' :
                [$status, $code] = $this->calAsianHandicap($this->bet, $fixture['response'][0], true);
                break;
            case 'under-over' :
                [$status, $code] = $this->execUnderOverBet($this->bet, $fixture['response'][0], false);
                break;
            case 'under-over-1h' :
                [$status, $code] = $this->execUnderOverBet($this->bet, $fixture['response'][0], true);
                break;
            case '1x2' :
                [$status, $code] = $this->exec1X2Bet($this->bet, $fixture['response'][0], false);
                break;
            case '1x2-1h' :
                [$status, $code] = $this->exec1X2Bet($this->bet, $fixture['response'][0], true);
                break;
        }

        $user = User::query()->where('id', $this->bet->user_id)->first();
        $currentPoint = $user->point;

        if ($status == 'true') {
            if ($code == 'win') {
                $amountWin = abs($this->bet->amount * $this->bet->odd_value);
                User::query()->where('id', $this->bet->user_id)->update([
                    'point' => intval($currentPoint + $amountWin),
                ]);
                Bet::query()->where('id', $this->bet->id)->update([
                    'status' => BetStatus::WIN,
                    'budget_in_out' => $amountWin
                ]);
            } elseif ($code == 'lose') {
                User::query()->where('id', $this->bet->user_id)->update([
                    'point' => max(intval($currentPoint - $this->bet->amount), 0),
                ]);
                Bet::query()->where('id', $this->bet->id)->update([
                    'status' => BetStatus::LOSE,
                    'budget_in_out' => $this->bet->amount * (-1)
                ]);
            } elseif ($code == 'draw') {
                Bet::query()->where('id', $this->bet->id)->update([
                    'status' => BetStatus::DRAW,
                    'budget_in_out' => 0
                ]);
            }
        }

        \Log::info($status.'-'.$code);
    }
}
