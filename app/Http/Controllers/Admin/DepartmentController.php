<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Department\CreateDepartmentRequest;
use App\Http\Requests\Admin\Department\EditDepartmentRequest;
use App\Http\Requests\Admin\Department\ListDepartmentRequest;
use App\Models\Department;
use App\Models\Permission;
use App\Services\Admin\Department\CreateDepartmentService;
use App\Services\Admin\Department\DeleteDepartmentService;
use App\Services\Admin\Department\DetailDepartmentService;
use App\Services\Admin\Department\EditDepartmentService;
use App\Services\Admin\Department\ListDepartmentService;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ListDepartmentRequest $request)
    {
        $departments = resolve(ListDepartmentService::class)->setRequest($request)->handle();
        $permissions = Permission::query()->get();

        return view('admin.department.index', compact(['departments', 'permissions']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::query()->get();

        return view('admin.department.create', compact(['permissions']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDepartmentRequest $request)
    {
        return resolve(CreateDepartmentService::class)->setRequest($request)->handle();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permissions = Permission::query()->get();
        $department = resolve(DetailDepartmentService::class)->getDepartmentById($id)->handle();
        $departmentPermission = $department->permissions->pluck('id')->toArray();

        return view('admin.department.edit', compact(['department', 'permissions', 'departmentPermission']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditDepartmentRequest $request, $id)
    {
        return resolve(EditDepartmentService::class)->setDepartmentById($id)->setRequest($request)->handle();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return resolve(DeleteDepartmentService::class)->setDepartmentById($id)->handle();
    }
}
