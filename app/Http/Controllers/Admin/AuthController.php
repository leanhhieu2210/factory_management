<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\LoginRequest;
use App\Services\Admin\LoginService;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function loginForm()
    {
        return view('admin.login');
    }

    public function login(LoginRequest $request)
    {
        return resolve(LoginService::class)->setRequest($request)->handle();
    }

    public function logout() {
        Auth::guard('admins')->logout();

        return redirect()->route('admin.login.form');
    }
}
