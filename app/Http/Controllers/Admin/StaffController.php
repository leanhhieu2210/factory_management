<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Staff\CreateStaffRequest;
use App\Http\Requests\Admin\Staff\ListStaffRequest;
use App\Http\Requests\Admin\Staff\UpdateStaffRequest;
use App\Models\Department;
use App\Models\Position;
use App\Models\Staff;
use App\Services\Admin\Staff\CreateStaffService;
use App\Services\Admin\Staff\DeleteStaffService;
use App\Services\Admin\Staff\DetailStaffService;
use App\Services\Admin\Staff\ListStaffService;
use App\Services\Admin\Staff\UpdateStaffService;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ListStaffRequest $request)
    {
        $staffs = resolve(ListStaffService::class)->setRequest($request)->handle();

        return view('admin.staffs.index', compact(['staffs']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::query()->get();
        $positions = Position::query()->get();

        return view('admin.staffs.create', compact(['departments', 'positions']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStaffRequest $request)
    {
        return resolve(CreateStaffService::class)->setRequest($request)->handle();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $staff = resolve(DetailStaffService::class)->setData(['id' => $id])->handle();
        $departments = Department::query()->get();
        $positions = Position::query()->get();

        return view('admin.staffs.edit', compact(['staff', 'departments', 'positions']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStaffRequest $request, $id)
    {
        resolve(UpdateStaffService::class)->setStaffById($id)->setRequest($request)->handle();

        return redirect()->route('admin.staff.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return resolve(DeleteStaffService::class)->setStaffById($id)->handle();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function info($id)
    {
        $data = [];

        $staff = Staff::query()
            ->select('full_name', 'position')
            ->where('id', $id)
            ->whereNull('deleted_at')
            ->with('positionWork')
            ->first();

        if (!$staff) {
            return \Response::json(array("errors" => "false"), 400);
        }

        $data = [
            'full_name' => $staff->full_name,
            'position'  => $staff->positionWork->name,
        ];

        return \Response::json($data, 200);
    }

}
