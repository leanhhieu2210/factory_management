<?php

namespace App\Http\Requests\Admin\Department;

use Illuminate\Foundation\Http\FormRequest;

class EditDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:255'
            ],
            'permission' => [
                'array'
            ],
            'permission.*' => [
                'exists:permissions,id'
            ]
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('labels.department_name'),
        ];
    }
}
