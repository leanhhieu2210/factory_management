<?php

namespace App\Http\Requests\Admin\Staff;

use App\Rules\PhoneRule;
use Illuminate\Foundation\Http\FormRequest;

class CreateStaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name'       => [
                'required',
                'max:255',
            ],
            'phone_number'    => [
                'required',
                new PhoneRule(),
                'max:255',
            ],
            'cccd'            => [
                'required',
                'max:255',
            ],
            'date_of_birth'   => [
                'required',
                'date',
                'date_format:Y-m-d',
                'before:' . date('Y-m-d'),
            ],
            'sex'             => [
                'nullable',
            ],
            'address'         => [
                'nullable',
                'max:255',
            ],
            'date_start_work' => [
                'required',
                'date',
                'date_format:Y-m-d',
            ],
            'position'        => [
                'required',
            ],
            'department'      => [
                'required',
            ],
            'diligence'       => [
                'required',
                'max:20',
            ],
            'avatar'          => [
                'required',
                'file',
                'mimes:jpg,jpeg,png',
                'max:2048'
            ],
        ];
    }

    public function attributes()
    {
        return [
            'full_name'       => __('labels.full_name'),
            'phone_number'    => __('labels.phone_number'),
            'cccd'            => __('labels.cccd'),
            'date_of_birth'   => __('labels.date_of_birth'),
            'sex'             => __('labels.sex'),
            'address'         => __('labels.address'),
            'date_start_work' => __('labels.date_start_work'),
            'position'        => __('labels.position'),
            'department'      => __('labels.department'),
            'diligence'       => __('labels.diligence'),
            'avatar'          => __('labels.avatar'),
        ];
    }

    public function messages()
    {
        return [
            'avatar.max' => "Vui lòng chọn Ảnh đại diện trong vòng 2Mb.",
        ];
    }
}
