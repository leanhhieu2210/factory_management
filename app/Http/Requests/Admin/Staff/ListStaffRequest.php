<?php

namespace App\Http\Requests\Admin\Staff;

use Illuminate\Foundation\Http\FormRequest;

class ListStaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => [
                'nullable',
                'max:255'
            ],
            'phone_number' => [
                'nullable',
                'max:255'
            ],
            'limit' => [
                'nullable'
            ],
            'sort_rule' => [
                'nullable'
            ],
            'sort_column' => [
                'nullable'
            ],
        ];
    }
}
