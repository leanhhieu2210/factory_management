<?php

namespace App\Http\Requests\Admin\Staff;

use App\Rules\PhoneRule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateStaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name'       => [
                'required',
                'max:255',
            ],
            'phone_number'    => [
                'required',
                new PhoneRule(),
                'max:255',
            ],
            'cccd'            => [
                'required',
                'max:255',
            ],
            'date_of_birth'   => [
                'required',
                'date',
                'date_format:Y-m-d',
                'before:' . date('Y-m-d'),
            ],
            'sex'             => [
                'nullable',
            ],
            'address'         => [
                'nullable',
                'max:255',
            ],
            'date_start_work' => [
                'nullable',
                'date',
                'date_format:Y-m-d',
            ],
            'position'        => [
                'required',
            ],
            'department'      => [
                'required',
            ],
            'diligence'       => [
                'required',
                'max:20',
            ],
            'avatar'          => [
                'file',
                'mimes:jpg,jpeg,png',
                'max:2048'
            ],
        ];
    }
}
