<?php

namespace App\Console\Commands;

use App\Jobs\BetExecuteJob;
use App\Models\Bet;
use App\Models\User;
use Illuminate\Console\Command;

class BetExecute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:bet_execute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bet execute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bets = Bet::query()->select(['*'])->where('status', '=', 1)->get();
        foreach ($bets as $bet) {
            BetExecuteJob::dispatch($bet)->onQueue('bet_exec');
        }
    }
}
