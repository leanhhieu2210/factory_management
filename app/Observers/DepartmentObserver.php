<?php

namespace App\Observers;

use App\Models\Department;
use App\Models\PermissionDepartment;
use Carbon\Carbon;

class DepartmentObserver
{
    /**
     * Handle the Department "created" event.
     *
     * @param  \App\Models\Department  $department
     * @return void
     */
    public function created(Department $department)
    {
        if (request()->has('permission')) {
            foreach (request()->get('permission') as $value) {
                $data[] = [
                    'department_id' => $department->id,
                    'permission_id' => $value,
                    'created_at'    => Carbon::now(),
                ];
            }

            return PermissionDepartment::insert($data);
        }

        return true;
    }

    /**
     * Handle the Department "updated" event.
     *
     * @param  \App\Models\Department  $department
     * @return void
     */
    public function updated(Department $department)
    {
        PermissionDepartment::where('department_id', $department->id)->delete();

        foreach (request()->get('permission') as $value) {
            $data[] = [
                'department_id' => $department->id,
                'permission_id' => $value,
                'created_at'    => Carbon::now(),
            ];
        }

        PermissionDepartment::insert($data);
    }

    /**
     * Handle the Department "deleted" event.
     *
     * @param  \App\Models\Department  $department
     * @return void
     */
    public function deleted(Department $department)
    {
        PermissionDepartment::query()->where('department_id', $department->id)->delete();
    }

    /**
     * Handle the Department "restored" event.
     *
     * @param  \App\Models\Department  $department
     * @return void
     */
    public function restored(Department $department)
    {
        //
    }

    /**
     * Handle the Department "force deleted" event.
     *
     * @param  \App\Models\Department  $department
     * @return void
     */
    public function forceDeleted(Department $department)
    {
        //
    }
}
