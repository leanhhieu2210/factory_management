<?php

namespace App\Repositories;

use App\Models\WorkQuantity;
use HaiPG\LaravelCore\Core\BaseRepository;

class WorkQuantityRepository extends BaseRepository
{
    /**
     * Get the model of repository
     *
     * @return string
     */
    public function getModel()
    {
        return WorkQuantity::class;
    }

    /**
     * Register relations
     */
    public function allowRelations()
    {
        return [
            //
        ];
    }

    /**
     * OrderBy follow fields
     */
    public function getOrderableFields()
    {
        return [
            'created_at',
            'id',
        ];
    }
}
