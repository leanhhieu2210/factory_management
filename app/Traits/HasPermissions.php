<?php

namespace App\Traits;

use App\Models\Permission;

trait HasPermissions
{
    public function hasPermission($permission)
    {
        return (bool)$this->department->permissions->where('name_eng', $permission->name_eng)->count();
    }

}
