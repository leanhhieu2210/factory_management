<?php

if (!function_exists('active_menu')) {
    function active_menu($arrControler)
    {
        $action = app('request')->route()->getAction();

        if (isset($action['controller'])) {
            $controllerAction = class_basename($action['controller']);
            $controller = explode('@', $controllerAction);

            if (!empty($controller) && $controller[0] == $arrControler) {
                return "is-active";
            }
        }

        return "";
    }
}

if (!function_exists('active_sort')) {
    function active_sort($column)
    {
        $rs = '';

        if (request('sort_column') == $column) {
            $rs = 'is-active';
            $rs .= request('sort_rule') == 'ASC' ? ' is-asc' : '';
            $rs .= request('sort_rule') == 'DESC' ? ' is-desc' : '';
        }

        return $rs;
    }
}

if (!function_exists('statusColorClass')) {
    function statusColorClass($status)
    {
        if ($status == \App\Enums\BetStatus::PROCESSING) {
            return '#9d9d9d';
        }

        if ($status == \App\Enums\BetStatus::WIN) {
            return '#54b34e';
        }

        if ($status == \App\Enums\BetStatus::LOSE) {
            return '#cd2424';
        }

        if ($status == \App\Enums\BetStatus::DRAW) {
            return '#4c66ed';
        }

        if ($status == \App\Enums\BetStatus::FAILED) {
            return 'black';
        }
    }
}
