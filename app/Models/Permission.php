<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'name',
        'name_eng',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'permission_departments')->wherePivot('deleted_at', '=', null);
    }
}
