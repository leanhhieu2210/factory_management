<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkQuantity extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'date',
        'step_work_id',
        'staff_id',
        'quantity',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $table = 'work_quantitys';
}
