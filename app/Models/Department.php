<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function staffs()
    {
        return $this->hasMany(Staff::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_departments')->wherePivot('deleted_at', '=', null);
    }
}
