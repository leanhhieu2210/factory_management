<?php

namespace App\Models;

use App\Traits\HasPermissions;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Services\S3Service;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Staff extends Authenticatable
{
    use HasFactory, HasPermissions;

    const PATH_STAFF = "images/staffs";

    protected $appends =[
        'link_avatar',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'full_name',
        'username',
        'password',
        'phone_number',
        'cccd',
        'date_of_birth',
        'sex',
        'address',
        'avatar',
        'date_start_work',
        'position',
        'department_id',
        'diligence',
        'created_by',
        'updated_by',
        'deleted_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Get the staff's avatar.
     *
     * @return string
     */
    public function getLinkAvatarAttribute()
    {
        $s3 = resolve(S3Service::class);

        return (!empty($this->avatar)) ? 'storage/'. self::PATH_STAFF . '/' . $this->avatar : null;
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function positionWork()
    {
        return $this->belongsTo(Position::class, 'position', 'id');
    }
}
