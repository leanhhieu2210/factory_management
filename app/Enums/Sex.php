<?php

namespace App\Enums;

use HaiPG\LaravelCore\Contracts\EnumContract;

class Sex implements EnumContract
{
    const MALE = 0;
    const FEMALE = 1;
    const UNKNOWN = 2;

    /**
     * Get all constants
     */
    public static function all()
    {
        return [
            self::MALE    => 'Nam',
            self::FEMALE  => 'Nữ',
            self::UNKNOWN => 'Không xác định',
        ];
    }


    public static function label($sex)
    {
        $data = [
            self::MALE    => 'Nam',
            self::FEMALE  => 'Nữ',
            self::UNKNOWN => 'Không xác định',
        ];

        return $data[$sex];
    }
}
