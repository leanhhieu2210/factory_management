<?php

namespace App\Services\Admin\Work;

use App\Repositories\WorkQuantityRepository;
use Carbon\Carbon;
use HaiPG\LaravelCore\Core\BaseService;

class CreateWorkOutputService extends BaseService
{
    protected $collectsData = true;

    public function __construct(
        WorkQuantityRepository $repository
    )
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $data =  $this->setDataWork();

        $this->repository->insert($data);

        $this->alertAfterHandling('Báo cáo công việc thành công');

        return redirect()->route('admin.work.create');
    }

    private function setDataWork()
    {
        $rs = [];
        $now = Carbon::now()->format('Y-m-d');
        foreach ($this->data->get('datawork') as $key => $value) {
            if (!empty($value['step_work']) && !empty($value['quantity'])) {
                $rs[] = [
                    'date'          => $now,
                    'staff_id'      => $this->data->get('staff_id'),
                    'step_work_id'  => $value['step_work'],
                    'quantity'      => $value['quantity'],
                    'created_at'    => $now,
                    'created_by'    => auth()->user()->id,
                ];
            }
        }

        return $rs;
    }
}
