<?php

namespace App\Services\Admin;

use App\Repositories\AdminRepository;
use HaiPG\LaravelCore\Core\BaseService;
use Illuminate\Support\Facades\Auth;

class LoginService extends BaseService
{
    protected $collectsData = true;

    public function __construct(AdminRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $credentials = $this->data->only(['username', 'password']);

        if(Auth::guard('admins')->attempt($credentials->toArray(), false)) {
            return redirect()->route('admin.staff.index');
        }

        return redirect()->back()->withErrors(['msg' => 'wrong username or password'])
                                ->withInput(['username' => $this->data->get('username')]);
    }
}
