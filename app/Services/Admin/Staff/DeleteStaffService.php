<?php

namespace App\Services\Admin\Staff;

use App\Repositories\StaffRepository;
use HaiPG\LaravelCore\Core\BaseService;
use Symfony\Component\HttpFoundation\Response;

class DeleteStaffService extends BaseService
{
    protected $collectsData = true;

    public function __construct(
        StaffRepository $repository
    )
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        abort_if(!$this->staff, 404);

        $this->staff->delete();

        $this->alertAfterHandling('Xóa nhân viên thành công');

        return redirect()->route('admin.staff.index');
    }

    public function setStaffById($id)
    {
        $this->staff = $this->repository->firstWhere(['id' => $id]);

        return $this;
    }
}
