<?php

namespace App\Services\Admin\Staff;

use App\Models\Product;
use App\Models\Staff;
use App\Repositories\StaffRepository;
use App\Services\S3Service;
use HaiPG\LaravelCore\Core\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateStaffService extends BaseService
{
    protected $collectsData = true;

    public function __construct(
        StaffRepository  $repository,
        S3Service $S3
    )
    {
        $this->repository = $repository;
        $this->S3 = $S3;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->uploadImage('avatar', 'avatar');
            $this->data->put('username', $this->data->get('phone_number'));
            $this->data->put('password', Hash::make($this->data->get('phone_number')));
            $this->data->put('department_id', $this->data->get('department'));

            Staff::query()->create($this->data->toArray());

        });

        $this->alertAfterHandling('Đăng ký nhân viên thành công');

        return redirect()->route('admin.staff.index');
    }

    /**
     * Upload image to S3
     *
     * @param $columnName
     * @param $requestName
     */
    protected function uploadImage($columnName, $requestName)
    {
        $image = $this->data->get($requestName) ?? null;

        if (!empty($image)) {
            $resultUploading = $this->S3->putObjectFile("public/".Staff::PATH_STAFF, $image);

            if ($resultUploading['status']) {
                $this->data->put($columnName, $resultUploading['path']);
            }
        }
    }
}
