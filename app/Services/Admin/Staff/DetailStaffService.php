<?php

namespace App\Services\Admin\Staff;

use App\Models\Staff;
use App\Repositories\StaffRepository;
use HaiPG\LaravelCore\Core\BaseService;

class DetailStaffService extends BaseService
{
    protected $collectsData = true;

    public function __construct(
        StaffRepository $repository
    )
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $staff = $this->repository->scopeQuery(function ($query) {
            return $query->where('id', $this->data->get('id'));
        })->first();

        abort_if(!$staff, 404);

        return $staff;
    }
}
