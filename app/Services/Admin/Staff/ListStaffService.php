<?php

namespace App\Services\Admin\Staff;

use App\Repositories\StaffRepository;
use HaiPG\LaravelCore\Core\BaseService;

class ListStaffService extends BaseService
{
    protected $collectsData = true;

    public function __construct(StaffRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $this->repository->scopeQuery(function ($query) {
            return $query->when(!empty($this->data->get('full_name')), function ($q) {
                $q->where("full_name", "LIKE", "%".$this->data->get('full_name') ."%");
            })->when(!empty($this->data->get('phone_number')), function ($q) {
                $q->where("phone_number", "LIKE", "%" . $this->data->get('phone_number') ."%");
            })
            ->when($this->data->get('sort_column') && $this->data->get('sort_rule'), function ($q) {
                $q->orderBy($this->data->get('sort_column'), $this->data->get('sort_rule'));
            })
            ->orderBy('id', 'DESC');
        });

        $limit = $this->data->has('limit') ? $this->data->get('limit') : 20;

        return $this->repository->paginate($limit)->appends(request()->query());
    }
}
