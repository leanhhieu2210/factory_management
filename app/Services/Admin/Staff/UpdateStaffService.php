<?php

namespace App\Services\Admin\Staff;

use App\Models\Staff;
use App\Repositories\StaffRepository;
use App\Services\S3Service;
use HaiPG\LaravelCore\Core\BaseService;

class UpdateStaffService extends BaseService
{
    protected $collectsData = true;

    public function __construct(
        StaffRepository $repository,
        S3Service $S3
    )
    {
        $this->repository = $repository;
        $this->S3 = $S3;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        if(!$this->staff){
            $this->alertAfterHandling(
                "Không thể tải dữ liệu nhân viên, vui lòng kiểm tra",
                false
            );

            return false;
        }

        if ($this->data->has('avatar')) {
            $this->uploadImage('avatar', 'avatar');
        }
        $this->data->put('department_id', $this->data->get('department'));

        $this->staff->update($this->data->toArray());

        $this->alertAfterHandling('Sửa thông tin nhân viên thành công');
    }

    public function setStaffById($id) {
        $this->staff = $this->repository->firstWhere(['id' => $id]);

        return $this;
    }

    /**
     * Upload image to S3
     *
     * @param $columnName
     * @param $requestName
     */
    protected function uploadImage($columnName, $requestName)
    {
        $image = $this->data->get($requestName) ?? null;

        if (!empty($image)) {
            $resultUploading = $this->S3->putObjectFile("public/".Staff::PATH_STAFF, $image);

            if ($resultUploading['status']) {
                $this->data->put($columnName, $resultUploading['path']);
            }
        }
    }
}
