<?php

namespace App\Services\Admin\Department;

use App\Repositories\DepartmentRepository;
use Carbon\Carbon;
use HaiPG\LaravelCore\Core\BaseService;

class CreateDepartmentService extends BaseService
{
    protected $collectsData = true;

    public function __construct(
        DepartmentRepository $repository
    )
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $data = [
            'name'       => $this->data->get('name'),
            'created_at' => Carbon::now(),
        ];

        $this->repository->create($data);

        $this->alertAfterHandling('Đăng ký phòng ban thành công');

        return redirect()->route('admin.department.index');
    }
}
