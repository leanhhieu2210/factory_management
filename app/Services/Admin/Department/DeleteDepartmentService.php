<?php

namespace App\Services\Admin\Department;

use App\Repositories\DepartmentRepository;
use HaiPG\LaravelCore\Core\BaseService;

class DeleteDepartmentService extends BaseService
{
    protected $collectsData = true;

    public function __construct(
        DepartmentRepository $repository
    )
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        abort_if(!$this->department, 404);

        $this->department->delete();

        $this->alertAfterHandling('Xóa phòng ban thành công');

        return redirect()->route('admin.department.index');
    }

    public function setDepartmentById($id)
    {
        $this->department = $this->repository->firstWhere(['id' => $id]);

        return $this;
    }
}
