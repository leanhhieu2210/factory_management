<?php

namespace App\Services\Admin\Department;

use App\Models\Department;
use App\Repositories\DepartmentRepository;
use HaiPG\LaravelCore\Core\BaseService;
use Illuminate\Database\Eloquent\Model;

class DetailDepartmentService extends BaseService
{
    protected $collectsData = true;

    public function __construct(
        DepartmentRepository $repository
    )
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        abort_if(!$this->department, 404);

        return $this->department;
    }

    public function getDepartmentById($id)
    {
        $this->department = Department::query()
            ->where('id', $id)
            ->with(['permissions'])
            ->first();

        return $this;
    }
}
