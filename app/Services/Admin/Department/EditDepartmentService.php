<?php

namespace App\Services\Admin\Department;

use App\Repositories\DepartmentRepository;
use Carbon\Carbon;
use HaiPG\LaravelCore\Core\BaseService;

class EditDepartmentService extends BaseService
{
    protected $collectsData = true;

    public function __construct(
        DepartmentRepository $repository
    )
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        if(!$this->department){
            $this->alertAfterHandling(
                "Không thể tải dữ liệu phòng ban, vui lòng kiểm tra",
                false
            );

            return redirect()->back();
        }

        $this->data->put('updated_at', Carbon::now());

        $this->department->update($this->data->toArray());

        $this->alertAfterHandling('Sửa thông tin phòng ban thành công');

        return redirect()->route('admin.department.show', ['id' => $this->department->id]);
    }

    public function setDepartmentById($id)
    {

        $this->department = $this->repository->firstWhere(['id' => $id]);

        return $this;
    }
}
