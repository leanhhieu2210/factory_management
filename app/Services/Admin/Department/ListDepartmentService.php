<?php

namespace App\Services\Admin\Department;

use App\Http\Resources\ListDepartResource;
use App\Models\Department;
use App\Repositories\DepartmentRepository;
use HaiPG\LaravelCore\Core\BaseService;

class ListDepartmentService extends BaseService
{
    protected $collectsData = true;

    public function __construct(
        DepartmentRepository $repository
    )
    {
        $this->repository = $repository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $data = $this->repository->scopeQuery(function ($query) {
            return $query->select('*')
                        ->when(!empty($this->data->get('name')), function ($q) {
                            $q->where("name", "LIKE", "%".$this->data->get('name') ."%");
                        })
                        ->when(!empty($this->data->get('permission')), function ($q) {
                            $q->whereHas("permissions", function ($qr) {
                                return $qr->where('permissions.id', $this->data->get('permission'));
                            });
                        })
                        ->when($this->data->get('sort_column') && $this->data->get('sort_rule'), function ($q) {
                            $q->orderBy($this->data->get('sort_column'), $this->data->get('sort_rule'));
                        })
                        ->orderBy('id', 'ASC')
                        ->whereNull('deleted_at');
        })->with('permissions');

        return $this->compareNamePermission($data->all());
    }

    private function compareNamePermission($data) {
        if ($data->isEmpty()) {
            return $data;
        }

        $data->each(function ($value, $key){
            $listName = [];

            if ($value->permissions->isNotEmpty()) {
                $value->permissions->each(function ($valueP, $keyP) use (&$listName) {
                    $listName[] = $valueP->name;
                });

                $value['listPermission'] = join(', ', $listName);
            }
        });

        return $data;
    }
}
