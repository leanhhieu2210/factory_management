<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class S3Service
{
    protected $S3;
    protected $fileDriver;
    protected $basePrefix;

    public function __construct()
    {
        $this->fileDriver = config('filesystems.default');
        $this->basePrefix = config('filesystems.prefix');
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
    }

    public function putObjectFile($resource, $objectFile, $csv = false, $fileName = null)
    {
        $fileName = $fileName ?? time() . uniqid() . '.' . $objectFile->clientExtension();
        $savePath = $this->generatePath($resource) .'/'. $fileName;

        $upload = Storage::disk($this->fileDriver)->put($savePath, file_get_contents($objectFile));

        return [
            'status' => $upload,
            'path' => ($upload) ? ($csv ? $upload : $fileName) : ''
        ];
    }

    public function getObject($path)
    {
        $filePath = $this->generatePath($path);

        if (Storage::exists($filePath)) {
            return Storage::disk($this->fileDriver)->temporaryUrl(
                $filePath,
                Carbon::now()->addMinutes(10)
            );
        }
    }

    public function findObjectInPath($path, $name)
    {
        $filePath = $this->generatePath($path);

        $list = $this->S3->listContents($filePath);

        foreach ($list as $key => $data) {
            if ($data['basename'] == $name) {
                return true;
            }
        }
        return false;
    }

    public function deleteObject($path)
    {
        $this->S3->delete($path);
    }

    public function moveFile($path, $prefix = '')
    {
        $uploadPath = config('gmo.s3_folders.upload') . '/';
        $savePath   = config('gmo.s3_folders.save') . '/' . $prefix;

        $newFolder = str_replace($uploadPath, $savePath, $path);

        return $this->S3->move($path, $newFolder);
    }

    public function getUrl($filePath): string
    {
        return Storage::disk('s3')->temporaryUrl(
            $this->generatePath($filePath),
            Carbon::now()->addMinutes(30)
        );
    }

    /**
     * @param $filePath
     */
    public function delete($filePath)
    {
        $path = $this->generatePath($filePath);

        if (Storage::exists($path)) {
            Storage::delete($path);
        }
    }

    public function generatePath($path)
    {
        return !empty($this->basePrefix) ? "$this->basePrefix/$path" : $path;
    }

    /**
     * @param $file
     * @return string
     */
    public static function generateFileName($file)
    {
        return now()->timestamp . Str::uuid() . '.' . $file->getClientOriginalExtension();
    }
}
