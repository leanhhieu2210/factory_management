<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    "full_name"       => "Họ và tên",
    "phone_number"    => "Số điện thoại",
    "cccd"            => "Số căn cước",
    "date_of_birth"   => "Ngày sinh",
    "sex"             => "Giới tính",
    "address"         => "Địa chỉ",
    "date_start_work" => "Ngày bắt đầu công việc",
    "position"        => "Chức danh",
    "department"      => "Phòng ban",
    "diligence"       => "Chuyên cần",
    "avatar"          => "Ảnh đại diện",
    "department_name" => "Tên phòng ban",
];
