//modal
$(function () {
    $(".js-modalDeleteStaff").iziModal({
        overlayClose: false,
        borderBottom: false,
        headerColor: "rgb(0, 175, 102)",
    });

    $(".js-modalDeleteDepartment").iziModal({
        overlayClose: false,
        borderBottom: false,
        headerColor: "rgb(0, 175, 102)",
    });
});

$('.js-datepicker').datepicker({
    dateFormat: "yy-mm-dd",
    showButtonPanel: true,
    yearRange: "-100:+0",
    showOtherMonths: true,
    changeYear: true,
    changeMonth: true,
    regional: "vi",
    maxDate: -1,
});

$('.js-datepicker-work').datepicker({
    dateFormat: "yy-mm-dd",
    showButtonPanel: true,
    yearRange: "-100:+100",
    showOtherMonths: true,
    changeYear: true,
    changeMonth: true,
    regional: "vi",
});

//file upload & preview
$(function() {
    const isUpload = 'is-upload';
    const isFinish = 'is-finish';
    const upWrap = '.js-upload';
    const upInput = '.js-uploadInput';
    const upThumb = '.js-uploadThumb';
    const upDelete = '.js-uploadDelete';
    const upZoom = '.js-uploadZoom';
    const delImg = '.delete-image';

    //select file
    $(upInput).on('change', function() {
        const upParent = $(this).parents(upWrap);
        const upFile = $(this).prop('files')[0];

        if (upFile) {
            upParent.addClass(isUpload);
            $("#" + upParent.attr("data-img")).val(1);
            //thumb show & zoom href set
            const upReader = new FileReader();
            upReader.onload = function() {
                const upImg = $('<img>').attr('src', upReader.result);
                //upParent.find(upThumb).html(upImg);
                upParent.find(upThumb).addClass(isFinish);
                upParent.find(upThumb).css('background-image','url('+upReader.result+')');
                upParent.find(upZoom).attr('src', upReader.result);
            }
            upReader.readAsDataURL(upFile);
            upParent.find(delImg).val('');
        } else {
            upParent.find(upThumb).removeClass(isFinish).find('img').remove();
            upParent.removeClass(isUpload);
            upParent.find(upThumb).css('background-image','none');
        }
    });
    //delete file
    $(document).on('click', upDelete, function(){
        const upParent = $(this).parents(upWrap);
        $("#" + $(this).attr("data-img")).val(0);
        upParent.find(upThumb).removeClass(isFinish).find('img').remove();
        upParent.removeClass(isUpload);
        upParent.find(delImg).val($(this).attr("data-image"));
        upParent.find(upInput).val('');
    });
});

function onlyNumberKey(evt) {
// Only ASCII character in that range allowed
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}

function deleteStaff(e)
{
    $('#delete-staff').attr('action', $(e).attr('data-route'))
    $('.js-modalDeleteStaff').iziModal('open');
}

function deleteDepartment(e)
{
    $('#delete-department').attr('action', $(e).attr('data-route'))
    $('.js-modalDeleteDepartment').iziModal('open');
}
