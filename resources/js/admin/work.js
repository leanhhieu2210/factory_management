$(document).ready(function () {
    $('.staff_id').focusout(function () {
        const id = $(this).val();

        if (id != "") {
            var url = 'staff/' + id;

            $.ajax({
                method: "GET",
                url: url,
                data: {},
                'success' : function(data) {
                    $('.staff-name').text(data.full_name);
                    $('.position').text(data.position);
                },
                'error' : function(request,error)
                {
                    $('.staff-name').text('');
                    $('.position').text('');
                }
            });
        }
    });

    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});
