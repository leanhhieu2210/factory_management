

//sort
$(function() {
    const isActive = "is-active"
    const sort = '.js-sort';
    const searchForm = '#searchForm'
    const isAsc = 'is-asc';
    const isDesc = 'is-desc';
    $(document).on('click', sort, function() {
        var sort_column = $(this).attr('id');
        var c_sort_column = $(searchForm).data('sort_column');
        var sort_rule;

        if($(this).hasClass(isActive)){
            if($(this).hasClass(isAsc)){
                $(this).removeClass(isAsc).addClass(isDesc);
            } else {
                $(this).removeClass(isDesc).addClass(isAsc);
            }
        } else {
            $(sort).removeClass(isActive + ' ' + isAsc  + ' ' + isDesc);
            $(this).addClass(isActive + ' ' + isAsc);
        }

        if (sort_column == c_sort_column) {
            var c_sort_rule = $(searchForm).data('sort_rule');
            if (c_sort_rule == 'ASC') {
                sort_rule = 'DESC';
            } else {
                sort_rule = 'ASC';
            }
        } else {
            sort_rule = 'ASC';
        }

        $('<input>').attr({
            type: 'hidden',
            name: 'sort_column',
            value: sort_column
        }).appendTo($(searchForm));

        $('<input>').attr({
            type: 'hidden',
            name: 'sort_rule',
            value: sort_rule
        }).appendTo($(searchForm));

        $(searchForm).submit();
    });
});
