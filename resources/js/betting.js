$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
});

$(document).ready(function () {
    localStorage.removeItem('fixtureId')
    localStorage.removeItem('leagueId')
    localStorage.removeItem('betName')
    localStorage.removeItem('oddValue')
    localStorage.removeItem('oddRate')
    localStorage.removeItem('oddTarget')

    $('.bet-click').on('click', function () {
        let leagueId = $(this).data('leagueid')
        let leagueName = $(this).data('leaguename')
        let homeTeam = $(this).data('hometeam')
        let awayTeam = $(this).data('awayteam')
        let betName = $(this).data('betname')
        let oddValue = $(this).data('oddvalue')
        let oddRate = $(this).data('odd-rate')
        let oddTarget = $(this).data('oddtarget')
        let fixtureId = $(this).data('fixtureid')

        $('#odd-target-bet').text(oddTarget)
        $('#odd-value-bet').text('@' + oddValue)
        $('#odd-rate-bet').text(oddRate)
        $('#league-name-bet').text(leagueName)
        $('#home-team-bet').text(homeTeam)
        $('#away-team-bet').text(awayTeam)


        localStorage.setItem('fixtureId', fixtureId)
        localStorage.setItem('leagueId', leagueId)
        localStorage.setItem('betName', betName)
        localStorage.setItem('oddValue', oddValue)
        localStorage.setItem('oddRate', oddRate)
        localStorage.setItem('oddTarget', oddTarget)
    })

    $('#betting-submit-btn').on('click', function () {
        let amount = $('#bet-amount').val()
        $.ajax({
            url: $('#betting-url').val(),
            type: "POST",
            dataType: "json",
            async: false,
            data: {
                league_id: localStorage.getItem('leagueId'),
                fixture_id: localStorage.getItem('fixtureId'),
                bet_name: localStorage.getItem('betName'),
                odd_value: localStorage.getItem('oddValue'),
                odd_rate: localStorage.getItem('oddRate'),
                odd_target: localStorage.getItem('oddTarget'),
                amount: amount,
            },
            statusCode: {
                422: function (response) {
                    console.log(422)
                }
            },
            success: function (result) {

                console.log(result)

                localStorage.removeItem('fixtureId')
                localStorage.removeItem('leagueId')
                localStorage.removeItem('betName')
                localStorage.removeItem('oddValue')
                localStorage.removeItem('oddRate')
                localStorage.removeItem('oddTarget')

                $('#odd-target-bet').text('')
                $('#odd-value-bet').text('')
                $('#odd-rate-bet').text('')
                $('#league-name-bet').text('')
                $('#home-team-bet').text('')
                $('#away-team-bet').text('')

                $('#bet-amount').val('')
                $('#bet-amount').text('')

                $('#current-point').text(result.new_point)
                $('.point-header').text(result.new_point + 'K')
                alert('Bet Success !!!')
            }
        })
    })
})
