@extends('admin.layouts.master-login')
@section('content')
    <div class="login-logo">
        <img class="img-fluid" src="" alt="">
    </div>

    <form id="form-login" action="{{ route('admin.login.action') }}" method="POST">
        @csrf
        <div class="login-body">
            <p class="login-headline">Đăng Nhập</p>
            @if ($errors->any())
                <div class="message-box any-message">
                    @foreach ($errors->all() as $error)
                        <ul>
                            <li>{{ $error }}</li>
                        </ul>
                    @endforeach
                </div>
            @endif
            <div class="alert_err message-box" role="alert" style="display: none;">
                <ul class="alert_err_msg"></ul>
            </div>

            <div class="login-item">
                <ul class="login-form">
                    <li class="login-form-item">
                        <input class="formInput @error('username') error @enderror" type="text" name="username"
                            value="{{ old('username') }}" placeholder="Tên đăng nhập"
                            autocomplete="off" id="username">
                    </li>
                    <li class="login-form-item">
                        <input class="formInput @error('password') error @enderror" type="password" name="password"
                            placeholder="Mật khẩu" id="password">
                    </li>
                </ul>
            </div>
{{--            <div class="login-item">--}}
{{--                <label class="formCheck">--}}
{{--                    <input class="formCheck-input" type="checkbox" name="remember" value="1">--}}
{{--                    <span class="formCheck-label">{{ __('labels.user.input.auto_login') }}</span>--}}
{{--                </label>--}}
{{--            </div>--}}
            <div class="login-item">
                <button class="btn btn-block btn-main" type="submit">Đăng nhập</button>
            </div>
        </div>
    </form>
@endsection

@push('custom-scripts')

@endpush
