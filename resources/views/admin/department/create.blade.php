@extends('admin.layouts.master')
@section('title', 'Quản lý nhân viên')
@section('content')
    <div class="pageHeader">
        <div class="pageHeader-top">
            <button class="drawerBtn js-drTrigger">
                <em class="drawerBtn-icon fas fa-bars"></em>
                <span class="drawerBtn-text">MENU</span>
            </button>
            <h2 class="pageHeader-ttl">Đăng ký phòng ban</h2>
        </div>
        <ul class="breadcrumb">
            <li class="breadcrumb-item">
                <a class="breadcrumb-link" href="#" title="home">Home</a>
            </li>
            <li class="breadcrumb-item">Đăng ký phòng ban</li>
        </ul>
    </div>
    <div class="pageBody">
        <div class="panelRow">
            <nav class="pageNav">
                <ul class="pageNav-unit">
                    <li class="pageNav-item">
                        <a class="pageNav-link" href="{{ route('admin.department.index') }}">Danh sách phòng ban</a>
                    </li>
                    <li class="pageNav-item">
                        <a class="pageNav-link is-active" href="">Đăng ký phòng ban</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="panelRow">
            <div class="row">
                <div class="panelUnit col-12">
                    <section class="panel">
                        <form action="{{ route('admin.department.store')  }}" method="POST">
                            @csrf
                            <div class="panelHeader">
                                <h3 class="panelHeader-headline">Đăng ký phòng ban</h3>
                            </div>
                            <div class="panelBody">
                                <div class="alert_err alert-danger" role="alert" style="{{ $errors->any() ? '' : 'display: none' }}">
                                    <ul class="alert_err_msg">
                                        @if ($errors->any())
                                            @foreach (array_unique($errors->all()) as $key => $error)
                                                <li class="error">{{ $error }}</li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Tên phòng ban</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <input class="formInput @error('name') error @enderror"
                                                           type="text" name="name"
                                                           value="{{ old('name', request('name')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Quyền hạn</p>
                                                </div>
                                                <div class="formField">
                                                    <div class="formSelect">
                                                        <select class="formSelect-input js-permission"  name="permission[]" multiple="multiple">
                                                            @foreach($permissions as $permission)
                                                                <option {{  (old('permission') == $permission->id) ? 'selected' : '' }} value="{{ $permission->id }}">{{ $permission->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panelFooter text-center">
                                <button class="btn btn-black" type="submit">Đăng ký</button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('department-scripts')
    <script src="{{ mix('/js/admin/department.js') }}"></script>
@endpush
