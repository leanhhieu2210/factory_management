<nav class="globalNav js-drSlide">
    <div class="globalNav-scroll scroll-auto">
        <ul class="globalNav-unit">
            <li class="globalNav-item">
                <a class="globalNav-link {{ active_menu('PositionController') }}"
                   href="{{ route('admin.setting.position') }}">
                    <i class="globalNav-icon fas fa-cogs"></i>
                    Thiết Lập Chung</a>
            </li>
            <li class="globalNav-item">
                <a class="globalNav-link js-user-checked {{ active_menu('DepartmentController') }}"
                   href="{{ route('admin.department.index') }}">
                    <i class="globalNav-icon fas fa-sitemap"></i>Quản Lý Phòng Ban</a>
            </li>
            <li class="globalNav-item">
                <a class="globalNav-link js-user-checked {{ active_menu('StaffController') }}"
                   href="{{ route('admin.staff.index') }}">
                    <i class="globalNav-icon fas fa-users"></i>Quản Lý Nhân Viên</a>
            </li>
            <li class="globalNav-item">
                <a class="globalNav-link js-user-checked {{ active_menu('WorkController') }}"
                   href="{{ route('admin.work.create') }}">
                    <i class="globalNav-icon fas fa-coins"></i>Quản Lý Sản Lượng</a>
            </li>
        </ul>
    </div>
</nav>
