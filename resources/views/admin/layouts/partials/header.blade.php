<header class="globalHeader">
    <div class="globalHeader-unit">
        <h1 class="globalHeader-logo">
            <a class="globalHeader-logo-link" href="{{ route('admin.staff.index') }}">
                <img class="img-fluid img-logo" src="{{ mix('/images/admin/logo.png') }}" alt="ロゴ">
            </a>
        </h1>
        <a href="{{ route('admin.logout.action') }}" class="globalHeader-logout" type="submit">Đăng Xuất</a>
    </div>

    <div class="scrollHeader js-scrollHeader js-drSlide"></div>
</header>
