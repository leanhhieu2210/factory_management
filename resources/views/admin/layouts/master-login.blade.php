<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="">
    <link rel="shortcut icon" href="{{ asset('assets/admin/img/common/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{ asset('assets/admin/img/common/favicon.ico') }}">
    <title>Quản lý xưởng may | Đăng nhập</title>
    <!--[add file]-->
    <!--[/add file]-->
    <link href="{{ mix('/css/admin/reset.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/admin/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/admin/style.css') }}" rel="stylesheet">
</head>

<body>

    <div class="login">
        @yield('content')
    </div>

    <!--[js]-->
    <!--[/js]-->
    <!--[add js]-->
    <!--[/add js]-->

    @stack('plugin-scripts')
    @stack('custom-scripts')
</body>

</html>
