<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ mix('css/fontawesome/css/all.css') }}">
    <!--[add file]-->
    <!--[/add file]-->
    <link href="{{ mix('/css/admin/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/iziModal.min.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/jquery-ui.structure.min.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ mix('/css/admin/custom.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/admin/reset.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/admin/style.css') }}" rel="stylesheet">
    @stack('style')
</head>

<body>

<!--[header]-->
@include('admin.layouts.partials.header')
<!--[/header]-->

<!--[content]-->
<div class="globalWrap">
    <!--[global nav]-->
@include('admin.layouts.partials.global-nav')
<!--[/global nav]-->
    <div class="globalBody js-drSlide">
        <main class="globalMain">
            <!--[main]-->
        @yield('content')
        <!--[/main]-->
        </main>

        <!--[footer]-->
    @include('admin.layouts.partials.footer')
    <!--[/footer]-->
    </div>
</div>
<!--[/content]-->
<!--[js]-->
<script src="{{ mix('/js/libs/jquery.min.js') }}"></script>
<script src="{{ mix('js/libs/jquery-ui.min.js') }}"></script>
{{--<script src="{{ asset('assets/admin/libs/jquery/jquery.validate.js') }}"></script>--}}
{{--<script src="{{ asset('assets/admin/libs/lity/lity.js') }}"></script>--}}
<script src="{{ mix('js/libs/iziModal.min.js') }}"></script>
<script src="{{ mix('js/libs/jquery-ui.min.js') }}"></script>
<script src="{{ mix('js/libs/datepicker-vi.js') }}"></script>
<script src="{{ mix('js/admin/common.js') }}"></script>
{{--<script src="{{ asset('assets/admin/js/common.js') }}"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!--[/js]-->

@stack('scripts')
@stack('user-scripts')
@stack('work-scripts')
@stack('department-scripts')
</body>

</html>
