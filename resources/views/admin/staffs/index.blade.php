@extends('admin.layouts.master')
@section('title', 'Quản lý nhân viên')
@section('content')
    <div class="pageHeader">
        <div class="pageHeader-top">
            <button class="drawerBtn js-drTrigger">
                <em class="drawerBtn-icon fas fa-bars"></em>
                <span class="drawerBtn-text">MENU</span>
            </button>
            <h2 class="pageHeader-ttl">Quản lý nhân viên</h2>
        </div>
        <ul class="breadcrumb">
            <li class="breadcrumb-item">
                <a class="breadcrumb-link" href="#" title="home">Home</a>
            </li>
            <li class="breadcrumb-item">Quản lý nhân viên</li>
        </ul>
    </div>
    <div class="pageBody">
        <div class="panelRow">
            <nav class="pageNav">
                <ul class="pageNav-unit">
                    <li class="pageNav-item">
                        <a class="pageNav-link is-active" href="">Danh sách nhân viên</a>
                    </li>
                    <li class="pageNav-item">
                        <a class="pageNav-link" href="{{ route('admin.staff.create') }}">Đăng ký nhân viên</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="panelRow">
            <div class="row">
                <div class="panelUnit col-12">
                    <section class="panel">
                        <form action="{{ route('admin.staff.index')  }}" method="GET" id="searchForm"
                              data-sort_rule="{{ request('sort_rule') }}"
                              data-sort_column="{{ request('sort_column') }}"
                              data-searchs="{{ json_encode(request()->all()) }}">
                            <div class="panelHeader">
                                <h3 class="panelHeader-headline">Quản lý nhân viên</h3>
                            </div>
                            <div class="panelBody">
                                <div class="alert_err alert-danger" role="alert" style="{{ $errors->any() ? '' : 'display: none' }}">
                                    <ul class="alert_err_msg">
                                        @if ($errors->any())
                                            @foreach (array_unique($errors->all()) as $key => $error)
                                                <li class="error">{{ $error }}</li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
{{--                                @if (session()->has('success'))--}}
{{--                                    <div class="alert-suc alert-success" role="alert">--}}
{{--                                        <ul class="">--}}
{{--                                            <li class="">{{ session()->get('msg_success') }}</li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                @endif--}}
                                @if (session()->has('status'))
                                    <div class="{{ session()->get('status') ? 'alert-suc alert-success' : 'alert_err alert-danger' }}" role="alert">
                                        <ul class="{{ session()->get('status') ? '' : 'alert_err_msg' }}">
                                            <li class="">{{ session()->get('msg') }}</li>
                                        </ul>
                                    </div>
                                @endif
                                <div class="formRow-unit" style="background:#eff1f4;">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Họ và tên</p>
                                                </div>
                                                <div class="formField">
                                                    <input class="formInput @error('full_name') error @enderror"
                                                           type="text" name="full_name"
                                                           value="{{ old('full_name', request('full_name')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Số điện thoại</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <input class="formInput @error('phone_number') error @enderror"
                                                           type="text" name="phone_number"
                                                           value="{{ old('phone_number', request('phone_number')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panelFooter text-center">
                                <button class="btn btn-black" type="submit">Tìm kiếm</button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
        <div class="panelRow">
            <div class="row">
                <div class="panelUnit col-12">
                    <section class="panel">
                        <div class="panelBody">
                            <div class="tableBody">
                                <div class="tableScroll scroll-auto">
                                    <table class="table" aria-describedby="mydesc">
                                        <thead>
                                        <tr>
                                            <th class="">
                                                ID
                                            </th>
                                            <th class="mw-130 sortBtn js-sort {{ active_sort("full_name") }}" id="full_name">
                                                Họ và tên<em class="sortBtn-icon"></em>
                                            </th>
                                            <th class="mw-130">
                                                Số điện thoại
                                            </th>
                                            <th class="mw-130">
                                                Ngày sinh
                                            </th>
                                            <th class="mw-130">
                                                Giới tính
                                            </th>
                                            <th>
                                                Địa chỉ
                                            </th>
                                            <th>

                                            </th>
                                            <th>

                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($staffs as $staff)
                                                <tr class="text-center">
                                                    <td class="text-left">
                                                        {{ $staff->id }}
                                                    </td>
                                                    <td class="text-left">
                                                        {{ $staff->full_name }}
                                                    </td>
                                                    <td class="text-left">
                                                        {{ $staff->phone_number }}
                                                    </td>
                                                    <td class="text-left">
                                                        {{ \Carbon\Carbon::parse($staff->date_of_birth)->format('d/m/Y') }}
                                                    </td>
                                                    <td class="text-left">
                                                        {{ \App\Enums\Sex::label($staff->sex) }}
                                                    </td>
                                                    <td class="text-left">
                                                        {{ $staff->address }}
                                                    </td>
                                                    <td class="">
                                                        <a class="icon-link delete-staff" data-route="{{ route('admin.staff.destroy', ['id' => $staff->id]) }}" onclick="deleteStaff(this)">
                                                            <i class="fa-solid fa-trash"></i>
                                                        </a>
                                                    </td>
                                                    <td class="">
                                                        <a class="icon-link" href="{{ route('admin.staff.show', ['id' => $staff->id]) }}">
                                                            <i class="fa-solid fa-pen-to-square"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr class="text-center col-2">
                                                    <td colspan="10">Không có thông tin nhân viên phù hợp</td>
                                                </tr>
                                            @endforelse

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tableFooter">
                                <div class="tableInfo">
                                    <div class="tableInfo-item">
                                        <p>Tổng số ：{{ $staffs->total() }} <span></span>Nhân viên</p>
                                    </div>
                                    <div class="tableInfo-item">
                                        <div class="pager">
                                            <div class="pager-item">
                                                <button onclick="location.href='{{ $staffs->previousPageUrl() }}'"
                                                        class="pager-prev btn btn-black {{ $staffs->previousPageUrl() ? '' : 'd-none' }}"
                                                        type="button">Trang trước</button>
                                            </div>
                                            <div class="pager-item">
                                                <div class="pager-num">
                                                    <div class="pager-num-item">
                                                        <input class="pager-num-input formInput" data-min="1"
                                                               data-current="{{ request()->get('page') ? request()->get('page') : 1 }}"
                                                               data-max="" type="text" onkeypress="return onlyNumberKey(event)"
                                                               name="page"
                                                               value="{{ request()->get('page') ? request()->get('page') : 1 }}"
                                                               id="form_page">
                                                    </div>
                                                    <div class="pager-num-item">
                                                        <span class="pager-num-text">/</span>
                                                    </div>
                                                    <div class="pager-num-item">
                                                        <span class="pager-num-text">{{ $staffs->lastPage() }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pager-item">
                                                <button onclick="location.href='{{ $staffs->nextPageUrl() }}'"
                                                        class="pager-next btn btn-black {{$staffs->nextPageUrl() ? '' : 'd-none'}}"
                                                        type="button">Trang sau</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modalInit js-modalInit js-modalDeleteStaff" role="dialog">
                                <form method="POST" action="" id="delete-staff">
                                    @method('PUT')
                                    @csrf
                                    <div class="panel">
                                        <div class="panelHeader row mg-0">
                                            <div class="col col-md-12">
                                                <h3 class="panelHeader-headline text-center">Bạn chắc chắn muốn xóa nhân viên này không?</h3>
                                            </div>
                                        </div>
                                        <div class="panelFooter">
                                            <input type="hidden" class="userId" name="userId" value="">
                                            <ul class="btnUnit justify-content-center">
                                                <li class="btnUnit-item"><button class="btn btn-black button_accept" data-izimodal-close="" type="button">Hủy</button></li>
                                                <li class="btnUnit-item"><button class="btn btn-main button_accept" type="submit">Xóa</button></li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('user-scripts')
    <script src="{{ mix('/js/admin/sort.js') }}"></script>
@endpush
