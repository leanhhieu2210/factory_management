@extends('admin.layouts.master')
@section('title', 'Quản lý nhân viên')
@section('content')
    <div class="pageHeader">
        <div class="pageHeader-top">
            <button class="drawerBtn js-drTrigger">
                <em class="drawerBtn-icon fas fa-bars"></em>
                <span class="drawerBtn-text">MENU</span>
            </button>
            <h2 class="pageHeader-ttl">Đăng ký nhân viên</h2>
        </div>
        <ul class="breadcrumb">
            <li class="breadcrumb-item">
                <a class="breadcrumb-link" href="#" title="home">Home</a>
            </li>
            <li class="breadcrumb-item">Đăng ký nhân viên</li>
        </ul>
    </div>
    <div class="pageBody">
        <div class="panelRow">
            <nav class="pageNav">
                <ul class="pageNav-unit">
                    <li class="pageNav-item">
                        <a class="pageNav-link" href="{{ route('admin.staff.index') }}">Danh sách nhân viên</a>
                    </li>
                    <li class="pageNav-item">
                        <a class="pageNav-link is-active" href="">Đăng ký nhân viên</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="panelRow">
            <div class="row">
                <div class="panelUnit col-12">
                    <section class="panel">
                        <form action="{{ route('admin.staff.store')  }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="panelHeader">
                                <h3 class="panelHeader-headline">Đăng ký nhân viên</h3>
                            </div>
                            <div class="panelBody">
                                <div class="alert_err alert-danger" role="alert" style="{{ $errors->any() ? '' : 'display: none' }}">
                                    <ul class="alert_err_msg">
                                        @if ($errors->any())
                                            @foreach (array_unique($errors->all()) as $key => $error)
                                                <li class="error">{{ $error }}</li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Họ và tên</p>
                                                </div>
                                                <div class="formField">
                                                    <input class="formInput @error('full_name') error @enderror"
                                                           type="text" name="full_name"
                                                           value="{{ old('full_name', request('full_name')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Số điện thoại</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <input class="formInput @error('phone_number') error @enderror"
                                                           type="text" name="phone_number"
                                                           onkeypress="return onlyNumberKey(event)"
                                                           value="{{ old('phone_number', request('phone_number')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Số căn cước</p>
                                                </div>
                                                <div class="formField">
                                                    <input class="formInput @error('cccd') error @enderror"
                                                           type="text" name="cccd"
                                                           onkeypress="return onlyNumberKey(event)"
                                                           value="{{ old('cccd', request('cccd')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Ngày sinh</p>
                                                </div>
                                                <div class="formField">
                                                    <div class="formDate">
                                                        <input
                                                            class="formInput js-datepicker {{ ($errors->has('date_of_birth')) ? 'error' : ''}}"
                                                            type="text" name="date_of_birth" value="{{ (old('date_of_birth')) ? Illuminate\Support\Carbon::parse(old('date_of_birth'))->format('Y-m-d') : '' }}"  autocomplete="off" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Giới tính</p>
                                                </div>
                                                <div class="formField">
                                                    <div class="formSelect">
                                                        <select class="formSelect-input"  name="sex">
                                                            <option selected value="">Vui lòng chọn</option>
                                                            @foreach(App\Enums\Sex::all() as $key => $value)
                                                                <option {{ (!empty(old('sex')) && old('sex') == $key) ? 'selected' : '' }} value="{{ $key }}">{{ $value }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Địa chỉ</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <input class="formInput @error('address') error @enderror"
                                                           type="text" name="address"
                                                           value="{{ old('address', request('address')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Ngày bắt đầu công việc</p>
                                                </div>
                                                <div class="formField">
                                                    <div class="formDate">
                                                        <input
                                                            class="formInput js-datepicker-work {{ ($errors->has('date_start_work')) ? 'error' : ''}}"
                                                            type="text" name="date_start_work" value="{{ (old('date_start_work')) ? Illuminate\Support\Carbon::parse(old('date_start_work'))->format('Y-m-d') : '' }}"  autocomplete="off" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Chức danh</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <div class="formSelect">
                                                        <select class="formSelect-input @error('position') error @enderror"  name="position">
                                                            <option selected value="">Vui lòng chọn</option>
                                                            @foreach($positions as $position)
                                                                <option {{ (old('position') && old('position') == $position->id) ? 'selected' : '' }} value="{{ $position->id }}">{{ $position->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Phòng ban</p>
                                                </div>
                                                <div class="formField">
                                                    <div class="formSelect">
                                                        <select class="formSelect-input @error('department') error @enderror"  name="department">
                                                            <option selected value="">Vui lòng chọn</option>
                                                            @foreach($departments as $department)
                                                                <option {{ (old('department') && old('department') == $department->id) ? 'selected' : '' }} value="{{ $department->id }}">{{ $department->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Chuyên cần</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <input class="formInput @error('diligence') error @enderror"
                                                           type="text" name="diligence"
                                                           onkeypress="return onlyNumberKey(event)"
                                                           value="{{ old('diligence', request('diligence')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-12 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Ảnh đại diện</p>
                                                </div>
                                                <div class="formField">
                                                    <div class="d-flex flex-wrap gap-20">
                                                        <div for="image_first" class="formFile js-upload {{($errors->has('avatar')) ? 'error' : ''}}">
                                                            <input id="avatar" type="file" class="w-0 formFile-input js-uploadInput js-disabled" accept="image/*" name="avatar">
                                                            <label for="avatar" class="formFile-trigger ">
                                                                <span class="formFile-thumb js-uploadThumb"></span>
                                                            </label>
                                                            <ul class="formFile-action">
                                                                <li class="formFile-action-item"><button class="formFile-action-btn btn-file-action btn-red js-uploadDelete" id="del_img" type="button">Xóa</button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panelFooter text-center">
                                <button class="btn btn-black" type="submit">Đăng ký</button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('user-scripts')
    <script src="{{ mix('/js/admin/sort.js') }}"></script>
@endpush
