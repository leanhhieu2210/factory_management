@extends('admin.layouts.master')
@section('title', 'Quản lý nhân viên')
@section('content')
    <div class="pageHeader">
        <div class="pageHeader-top">
            <button class="drawerBtn js-drTrigger">
                <em class="drawerBtn-icon fas fa-bars"></em>
                <span class="drawerBtn-text">MENU</span>
            </button>
            <h2 class="pageHeader-ttl">Báo cáo sản lượng</h2>
        </div>
        <ul class="breadcrumb">
            <li class="breadcrumb-item">
                <a class="breadcrumb-link" href="#" title="home">Home</a>
            </li>
            <li class="breadcrumb-item">Báo cáo sản lượng</li>
        </ul>
    </div>
    <div class="pageBody">
        <div class="panelRow">
            <nav class="pageNav">
                <ul class="pageNav-unit">
                    <li class="pageNav-item">
                        <a class="pageNav-link is-active" href="">Báo cáo sản lượng</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="panelRow">
            <div class="row">
                <div class="panelUnit col-12">
                    <section class="panel">
                        <form action="{{ route('admin.work.store')  }}" method="POST" >
                            @csrf
                            <div class="panelHeader">
                                <h3 class="panelHeader-headline">Báo cáo sản lượng</h3>
                            </div>
                            <div class="panelBody">
                                <div class="alert_err alert-danger" role="alert" style="{{ $errors->any() ? '' : 'display: none' }}">
                                    <ul class="alert_err_msg">
                                        @if ($errors->any())
                                            @foreach (array_unique($errors->all()) as $key => $error)
                                                <li class="error">{{ $error }}</li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                @if (session()->has('status'))
                                    <div class="{{ session()->get('status') ? 'alert-suc alert-success' : 'alert_err alert-danger' }}" role="alert">
                                        <ul class="{{ session()->get('status') ? '' : 'alert_err_msg' }}">
                                            <li class="">{{ session()->get('msg') }}</li>
                                        </ul>
                                    </div>
                                @endif
                                <div class="formRow-unit" >
                                    <div class="row no-gutters">
                                        <div class="col-lg-8 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser" >
                                                    <p class="formLabel-text">Mã nhân viên</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <input class="formInput staff_id @error('staff_id') error @enderror"
                                                           type="text" name="staff_id"
                                                           onkeypress="return onlyNumberKey(event)"
                                                           value="{{ old('staff_id', request('staff_id')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="formRow">
                                                <div class="formField" style="margin-right:1px;">
                                                    <p>Tên nhân viên : <span class="staff-name"></span></p>
                                                    <p>Chức danh : <span class="position"></span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-8 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Công đoạn 1</p>
                                                </div>
                                                <div class="formField">
                                                    <div class="formSelect">
                                                        <select class="formSelect-input @error('datawork') error @enderror"  name="datawork[1][step_work]">
                                                            <option selected value="">Vui lòng chọn</option>
                                                            @foreach($stepWorks as $stepWork)
                                                                <option {{ (old('datawork') && old('datawork') == $stepWork->id) ? 'selected' : '' }} value="{{ $stepWork->id }}">{{ $stepWork->step_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Số Lượng 1</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <input class="formInput @error('datawork') error @enderror"
                                                           type="text" name="datawork[1][quantity]"
                                                           onkeypress="return onlyNumberKey(event)"
                                                           value="{{ old('datawork', request('datawork')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-8 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Công đoạn 2</p>
                                                </div>
                                                <div class="formField">
                                                    <div class="formSelect">
                                                        <select class="formSelect-input @error('datawork') error @enderror"  name="datawork[2][step_work]">
                                                            <option selected value="">Vui lòng chọn</option>
                                                            @foreach($stepWorks as $stepWork)
                                                                <option {{ (old('datawork') && old('datawork') == $stepWork->id) ? 'selected' : '' }} value="{{ $stepWork->id }}">{{ $stepWork->step_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Số Lượng 2</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <input class="formInput @error('datawork') error @enderror"
                                                           type="text" name="datawork[2][quantity]"
                                                           onkeypress="return onlyNumberKey(event)"
                                                           value="{{ old('datawork', request('datawork')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-8 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Công đoạn 3</p>
                                                </div>
                                                <div class="formField">
                                                    <div class="formSelect">
                                                        <select class="formSelect-input @error('datawork') error @enderror"  name="datawork[3][step_work]">
                                                            <option selected value="">Vui lòng chọn</option>
                                                            @foreach($stepWorks as $stepWork)
                                                                <option {{ (old('datawork') && old('datawork') == $stepWork->id) ? 'selected' : '' }} value="{{ $stepWork->id }}">{{ $stepWork->step_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Số Lượng 3</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <input class="formInput @error('datawork') error @enderror"
                                                           type="text" name="datawork[3][quantity]"
                                                           onkeypress="return onlyNumberKey(event)"
                                                           value="{{ old('datawork', request('datawork')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-8 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Công đoạn 4</p>
                                                </div>
                                                <div class="formField">
                                                    <div class="formSelect">
                                                        <select class="formSelect-input @error('datawork') error @enderror"  name="datawork[4][step_work]">
                                                            <option selected value="">Vui lòng chọn</option>
                                                            @foreach($stepWorks as $stepWork)
                                                                <option {{ (old('datawork') && old('datawork') == $stepWork->id) ? 'selected' : '' }} value="{{ $stepWork->id }}">{{ $stepWork->step_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Số Lượng 4</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <input class="formInput @error('datawork') error @enderror"
                                                           type="text" name="datawork[4][quantity]"
                                                           onkeypress="return onlyNumberKey(event)"
                                                           value="{{ old('datawork', request('datawork')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="formRow-unit">
                                    <div class="row no-gutters">
                                        <div class="col-lg-8 col-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Công đoạn 5</p>
                                                </div>
                                                <div class="formField">
                                                    <div class="formSelect">
                                                        <select class="formSelect-input @error('datawork') error @enderror"  name="datawork[5][step_work]">
                                                            <option selected value="">Vui lòng chọn</option>
                                                            @foreach($stepWorks as $stepWork)
                                                                <option {{ (old('datawork') && old('datawork') == $stepWork->id) ? 'selected' : '' }} value="{{ $stepWork->id }}">{{ $stepWork->step_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-12">
                                            <div class="formRow">
                                                <div class="formLabel formLabelUser">
                                                    <p class="formLabel-text">Số Lượng 5</p>
                                                </div>
                                                <div class="formField" style="margin-right:1px;">
                                                    <input class="formInput @error('datawork') error @enderror"
                                                           type="text" name="datawork[5][quantity]"
                                                           onkeypress="return onlyNumberKey(event)"
                                                           value="{{ old('datawork', request('datawork')) }}"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panelFooter text-center">
                                <button class="btn btn-black" type="submit">Hoàn thành</button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('work-scripts')
    <script src="{{ mix('/js/admin/work.js') }}"></script>
@endpush
