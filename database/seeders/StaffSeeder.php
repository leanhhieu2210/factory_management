<?php

namespace Database\Seeders;

use App\Models\Staff;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Staff::query()->truncate();

        $data = [
            'full_name' => 'Admin',
            'username' => 'admin@abc',
            'password' => Hash::make('abc@1234'),
            'date_of_birth' => Carbon::now(),
            'date_start_work' => Carbon::now(),
            'position' => 1,
            'department_id' => 1,
            'created_at' => Carbon::now(),
        ];

        Staff::query()->insert($data);
    }
}
