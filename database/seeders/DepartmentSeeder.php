<?php

namespace Database\Seeders;

use App\Models\Department;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        Department::query()->truncate();

        $data = [
            [
                'name'       => 'Ban giám đốc',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ];

        Department::query()->insert($data);
    }
}
