<?php

namespace Database\Seeders;

use App\Models\Position;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        Position::query()->truncate();

        $data = [
            [
                'name'       => 'Giám đốc',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'name'       => 'Quản lý',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'name'       => 'Nhân viên',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ];

        Position::query()->insert($data);
    }
}
