<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name'     => 'Admin',
            'username' => 'admin@abc',
            'password' => Hash::make('abc@1234'),
        ];

        Admin::query()->insert($data);
    }
}
