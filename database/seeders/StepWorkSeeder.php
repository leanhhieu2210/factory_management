<?php

namespace Database\Seeders;

use App\Models\Staff;
use App\Models\StepWork;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class StepWorkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StepWork::query()->truncate();

        $data = [
            [
                'step_name' => 'Vắt sổ thân sau',
            ],
            [
                'step_name' => 'Vắt sổ tay vào thân (vắt sổ tra tay)',
            ],
            [
                'step_name' => 'Vắt sổ vai tay (vắt sổ sườn)',
            ],
            [
                'step_name' => 'Vắt sổ sóng mũ',
            ],
            [
                'step_name' => 'Vắt sổ thun mũ',
            ],
            [
                'step_name' => 'Vắt sổ mũ vào thân + nhãn',
            ],
            [
                'step_name' => 'Vắt sổ giàng trong',
            ],
            [
                'step_name' => 'Vắt sổ tất',
            ],
            [
                'step_name' => 'Tra tất vào thân',
            ],
            [
                'step_name' => 'Tra dây kéo hoàn chỉnh',
            ],
            [
                'step_name' => 'Vắt sổ thun tay',
            ],
            [
                'step_name' => 'May 1 đoạn thân trước',
            ],
            [
                'step_name' => 'Gấp + lộn TP',
            ],
            [
                'step_name' => 'Gấp TP',
            ],
            [
                'step_name' => 'Lộn TP',
            ],
            [
                'step_name' => 'Vắt sổ sóng thân sau + một đoạn thân trước',
            ],
        ];

        StepWork::query()->insert($data);
    }
}
