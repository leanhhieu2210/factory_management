<?php

namespace Database\Seeders;

use App\Models\Permission;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        Permission::query()->truncate();

        $data = [
            [
                'name'     => 'Quản lý nhân viên',
                'name_eng' => 'staff_management',
            ],
            [
                'name'     => 'Thiết lập chung',
                'name_eng' => 'setting_general',
            ],
            [
                'name'     => 'Quản lý sản lượng',
                'name_eng' => 'work_output',
            ],
            [
                'name'     => 'Quản lý phòng ban',
                'name_eng' => 'department_management',
            ],
        ];

        Permission::query()->insert($data);
    }
}
