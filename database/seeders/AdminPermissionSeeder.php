<?php

namespace Database\Seeders;

use App\Models\PermissionDepartment;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AdminPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        PermissionDepartment::query()->truncate();

        $data = [
            [
                'department_id' => 1,
                'permission_id' => 1,
                'created_at'    => $now,
            ],
            [
                'department_id' => 1,
                'permission_id' => 2,
                'created_at'    => $now,
            ],
            [
                'department_id' => 1,
                'permission_id' => 3,
                'created_at'    => $now,
            ],
            [
                'department_id' => 1,
                'permission_id' => 4,
                'created_at'    => $now,
            ],
        ];

        PermissionDepartment::query()->insert($data);
    }
}
