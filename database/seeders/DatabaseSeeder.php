<?php

namespace Database\Seeders;

use App\Models\StepWork;
use App\Repositories\StaffRepository;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeeder::class,
            AdminPermissionSeeder::class,
            DepartmentSeeder::class,
            PermissionSeeder::class,
            PositionSeeder::class,
            StaffSeeder::class,
            StepWorkSeeder::class,
        ]);
    }
}
