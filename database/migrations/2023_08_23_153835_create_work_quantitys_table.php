<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkQuantitysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_quantitys', function (Blueprint $table) {
            $table->id();
            $table->date('date')->nullable(false);
            $table->integer('step_work_id', false)->nullable(false);
            $table->integer('staff_id', false)->nullable(false);
            $table->integer('quantity', false)->nullable(false);
            $table->integer('created_by', false)->nullable(false);
            $table->integer('updated_by',false)->nullable();
            $table->integer('deleted_by',false)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_quantitys');
    }
}
