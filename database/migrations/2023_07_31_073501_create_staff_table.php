<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->id();
            $table->string('full_name')->nullable(false);
            $table->string('phone_number',20)->nullable(false);
            $table->string('cccd')->nullable(false);
            $table->date('date_of_birth')->nullable(false);
            $table->tinyInteger('sex', false)->comment("0:male, 1:female, 2:unknown")->nullable(false);
            $table->string('address')->nullable();
            $table->string('avatar')->nullable();
            $table->date('date_start_work')->nullable();
            $table->integer('position', false)->nullable(false);
            $table->integer('department', false)->nullable(false);
            $table->bigInteger('diligence', false)->nullable(false);
            $table->integer('created_by', false)->nullable();
            $table->integer('updated_by',false)->nullable();
            $table->integer('deleted_by',false)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
