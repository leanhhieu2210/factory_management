<?php

return [
    'base_uri' => env('FOOTBALL_BASE_URI', 'https://v3.football.api-sports.io'),
    'headers'  => [
        'x-rapidapi-host' => env('FOOTBALL_HOST', 'v3.football.api-sports.io'),
        'x-rapidapi-key'  => env('FOOTBALL_API_TOKEN', 'b424128c0dcb7fc1303222e48972739d'),
    ]
];
